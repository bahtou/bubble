const webpack = require('webpack');

const {
  contentBasePath, cssPaths, host,
  outputPath, port, postcssPaths
} = require('../base-params');


module.exports = {
  devtool: 'eval-source-map',

  devServer: {
    https: false,
    host,
    port,

    open: false,                  // Open the page in browser
    contentBase: contentBasePath, // Content not from webpack is served from here

    historyApiFallback: true,
    compress: true,

    hot: true,

    // Don't refresh if hot loading fails. Good while
    // implementing the client interface.
    hotOnly: true,
    inline: true,

    //capturing compilation related warnings and errors
    overlay: true,

    publicPath: '/',
    // --progress - [assets, children, chunks, colors, errors, hash, timings, version, warnings]
    stats: {
      assets: true,

      // Add build date and time information
      builtAt: true,

      // Add information about cached (not built) modules
      cached: true,

      // Show cached assets (setting this to `false` only shows emitted files)
      cachedAssets: true,

      children: true,
      chunks: false,
      colors: true,

      // Display the distance from the entry point for each module
      depth: false,
      // Display the entry points with the corresponding bundles
      entrypoints: false,

      errors: true,
      errorDetails: true, //depends on {errors: true}
      hash: true,
      modules: false,
      moduleTrace: true,
      performance: true,
      providedExports: true,
      publicPath: true,
      reasons: true,
      source: true,
      usedExports: false,
      timings: true,
      version: true,
      warnings: true
    }
  },

  module: {
    rules: [
      // {
      //   test: /\.scss$/,
      //   include: cssPaths,
      //   use: [
      //     {
      //       loader: 'style-loader',
      //       options: {
      //         sourceMap: true
      //       }
      //     },
      //     {
      //       loader: 'css-loader',
      //       options: {
      //         modules: true,
      //         // localIdentName: '[folder]__[local]--[hash:base64:10]',
      //         localIdentName: '[folder]__[local]',
      //         sourceMap: true,
      //         importLoaders: 1,
      //         import: false,
      //         url: false
      //       }
      //     },
      //     {
      //       loader: 'sass-loader',
      //       options: {
      //         sourceMap: true,
      //         includePaths: cssPaths,
      //         outputStyle: 'expanded',
      //         precision: 8
      //       }
      //     }
      //   ]
      // },
      {
        test: /\.css$/,
        include: cssPaths,
        use: [
          {
            loader: 'style-loader',
            options: {
              sourceMap: true
            }
          },
          {
            loader: 'css-loader',
            options: {
              modules: true,
              // localIdentName: '[folder]__[local]--[hash:base64:10]',
              localIdentName: '[folder]__[local]',
              sourceMap: true,
              importLoaders: 1,
              import: false,
              url: false
            }
          },
          {
            loader: 'postcss-loader',
            options: {
              sourceMap: true,
              plugins: loader => [
                require('postcss-smart-import')({
                  root: postcssPaths,
                  path: ['assets', 'components'],
                  skipDuplicates: true
                }),
                require('postcss-cssnext')({
                  // https://github.com/MoOx/postcss-cssnext/blob/master/src/features.js
                  features: {
                    customProperties: {
                      strict: false,
                      warnings: true,
                      preserve: false
                    },
                    applyRule: true,
                    calc: true,
                    imageSet: true,
                    nesting: true,
                    customMedia: {
                      extensions: {},
                      preserve: false,
                      appendExtensions: false // 'truthy'-based on preserve
                    },
                    mediaQueriesRange: true,
                    customSelectors: true,
                    pseudoClassMatches: {
                      lineBreak: true
                    },
                    autoprefixer: true
                  }
                }),
                require('precss') // SASS-like markup
              ]
            }
          }
        ]
      }
    ]
  },

  optimization: {
    namedModules: true,
    namedChunks: true
  },

  plugins: [
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify('development')
      }
    }),

    // Enable the plugin to let webpack communicate changes
    // to WDS. --hot sets this automatically!
    // Enable multi-pass compilation for enhanced performance
    // in larger projects. Good default.
    new webpack.HotModuleReplacementPlugin({
      multiStep: false
    })
  ]
};
