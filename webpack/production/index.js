const path = require('path');
const webpack = require('webpack');

const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const GitRevisionPlugin = require('git-revision-webpack-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const cssnano = require('cssnano');
const UglifyWebpackPlugin = require('uglifyjs-webpack-plugin');

const { cssPaths, postcssPaths } = require('../base-params');


module.exports = {
  entry: {
    vendor: [
      'react-hot-loader', 'react', 'react-dom', 'prop-types', 'babel-polyfill'
    ]
  },

  recordsPath: path.resolve(__dirname, '..', '..', './records.json'),

  module: {
    rules: [
      {
        test: /\.css$/,
        include: cssPaths,
        use: [
          MiniCssExtractPlugin.loader,
          {
            loader: 'css-loader',
            options: {
              module: true,
              localIdentName: '[folder]__[local]--[hash:base64:4]',
              sourceMap: true,
              importLoaders: 1,
              import: false,
              url: true
            }
          },
          {
            loader: 'postcss-loader',
            options: {
              sourceMap: true,
              plugins: (loader) => [
                require('postcss-smart-import')({
                  root: postcssPaths,
                  path: ['assets', 'components'],
                  skipDuplicates: true
                }),
                require('postcss-cssnext')({
                  // https://github.com/MoOx/postcss-cssnext/blob/master/src/features.js
                  features: {
                    customProperties: {
                      strict: false,
                      warnings: true,
                      preserve: false
                    },
                    applyRule: true,
                    calc: true,
                    imageSet: true,
                    nesting: true,
                    customMedia: {
                      extensions: {},
                      preserve: false,
                      appendExtensions: false // 'truthy'-based on preserve
                    },
                    mediaQueriesRange: true,
                    customSelectors: true,
                    pseudoClassMatches: {
                      lineBreak: true
                    },
                    pseudoClassNot: true,
                    autoprefixer: true
                  }
                }),
                require('precss') // SASS-like markup
              ]
            }
          }
        ]
      }
    ]
  },

  optimization: {
    concatenateModules: true,

    minimizer: [
      new UglifyWebpackPlugin({
        sourceMap: true,
        extractComments: false
        // uglifyOptions: {
        //   output: {
        //     comments: true,
        //     beautify: true
        //   }
        // }
      })
      // new UglifyWebpackPlugin({
      //   parallel: true,
      //   sourceMap: true,
      //   extractComments: false,
      //   uglifyOptions: {
      //     ecma: 6,
      //     mangle: false,
      //     output: {
      //       comments: false,
      //       beautify: true
      //     },
      //     compress: {
      //       warnings: false,
      //       drop_console: false,
      //       drop_debugger: true,
      //       dead_code: true
      //     },
      //     warnings: true
      //   }
      // })
    ],

    runtimeChunk: {
      name: 'manifest'
    },

    splitChunks: {
      // chunks: "initial",
      cacheGroups: {
        commons: {
          test: /[\\/]node_modules[\\/]/,
          name: 'vendor',
          chunks: 'initial',
        }
      }
    }
  },

  performance: {
    hints: "warning",         // "error" or false are valid too
    maxEntrypointSize: 50000, // in bytes, default 250k
    maxAssetSize: 450000,     // in bytes
  },

  plugins: [
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify('production')
      }
    }),

    new MiniCssExtractPlugin({
      filename: "[name].[contenthash:4].css",
      chunkFilename: "[name].[contenthash:4].[id].css"
    }),

    new webpack.BannerPlugin({
      banner: new GitRevisionPlugin().version(),
    }),

    new OptimizeCSSAssetsPlugin({
      cssProcessor: cssnano,
      cssProcessorOptions: {
        discardComments: {
          removeAll: false
        },
        // Run cssnano in safe mode to avoid
        // potentially unsafe transformations.
        safe: true,
      },
      canPrint: true // set 'false' for stats analysis
    }),

    // new webpack.optimize.AggressiveSplittingPlugin({
    //   minSize: 10000,
    //   maxSize: 30000,
    // }),
  ]
};
