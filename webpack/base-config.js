const webpack = require('webpack');
const webpackMerge = require('webpack-merge');

const {
  appEntry, contextPath, fontPathEntry,
  imagePathEntry, jsPaths, outputPath,
  resolveAliasPaths, resolveAliasModules
} = require('./base-params');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ErrorOverlayPlugin = require('error-overlay-webpack-plugin');
const CaseSensitivePathsPlugin = require('case-sensitive-paths-webpack-plugin');

const baseConfig = {
  context: contextPath,

  entry: {
    app: [ appEntry ]
  },

  output: {
    path: outputPath,
    publicPath: './',
    filename: '[name].[hash:4].js',
    chunkFilename: '[name].[chunkhash:4].js'
  },

  module: {
    rules: [
      {
        test: /\.js$/,
        include: jsPaths,
        use: [
          {
            loader: 'babel-loader',
            options: {
              // This is a feature of `babel-loader` for Webpack (not Babel itself).
              // It enables caching results in ./node_modules/.cache/babel-loader/
              // directory for faster rebuilds.
              cacheDirectory: true
            }
          },
          // {
          //   loader: 'eslint-loader',
          //   options: {
          //     fix: false,
          //     cache: './.cache',
          //     emitError: false, // 'true' stops webpack from bundling
          //     emitWarning: true,
          //     quiet: false,
          //     failOnWarning: false,
          //     failOnError: false
          //     // formatter(results) {
          //     //   console.log(results);

          //     //   return results.toString();
          //     // }
          //   }
          // }
        ]
      },
      {
        test: /\.(eot|ttf|otf|woff|woff2)$/,
        include: fontPathEntry,
        use: [{
          loader: 'url-loader',
          options: {
            // Limit at 50Kb. Above that it emits separate files
            limit: 50000,
            name: './fonts/[name].[hash:4].[ext]'
          }
        }]
      },
      {
        test: /\.(png|svg|jpg)$/,
        include: imagePathEntry,
        use: [{
          loader: 'url-loader',
          options: {
            // Limit at 25Kb. Above that it emits separate files
            limit: 25000,
            name: './images/[name].[hash:4].[ext]'
          }
        }]
      }
    ]
  },

  optimization: {
    noEmitOnErrors: true
  },

  plugins: [
    new ErrorOverlayPlugin(),

    new CaseSensitivePathsPlugin({
      debug: false
    }),

    new HtmlWebpackPlugin({
      title: 'react ❤ webpack',
      template: 'index.html',
      // favicon: '',
      // meta: { viewport: 'width=device-width, initial-scale=1, shrink-to-fit=no' },
      // minify: {Boolean|Object}
      inject: true,
      hash: true,
      cache: true,
      showErrors: true,
      chunksSortMode: 'dependency'
    }),

    //see webpack.EnvironmentPlugin(["NODE_ENV"])
    new webpack.DefinePlugin({
      'HOLA': JSON.stringify('hola desde el base-config')
    })
  ],

  resolve: {
    alias: resolveAliasPaths,
    extensions: ['.js'],
    enforceExtension: false,
    modules: resolveAliasModules
  }
};


module.exports = baseConfig;
