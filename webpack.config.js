const webpackMerge = require('webpack-merge');
const baseConfig = require('./webpack/base-config');


module.exports = mode => {
  const envConfig = require(`./webpack/${mode}`);

  return webpackMerge(baseConfig, envConfig, { mode });
};
