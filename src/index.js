import React from 'react';
import { render } from 'react-dom';
import HotModule from './pages';

import '!style-loader!css-loader!Styles/normalizer.8.0.0.css';
import '!style-loader!css-loader!Styles/globals.css';
import '!style-loader!css-loader!Styles/typography.css';


render(
  <HotModule />,
  document.getElementById('root-entry')
);
