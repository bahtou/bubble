import React from 'react';


import { container, description, displayLink, header, body, hzLine, titleLink } from './styles.css';
import { H1 } from 'Elements/Headers';


const Card = ({ title, link, desc }) => {
  return (
    <div className={ container }>
      <header className={ header }>
        <h2><a href={ link } className={ titleLink }>{ title }</a></h2>
      </header>
      <hr className={ hzLine } />
      <div className={ body }>
        <p className={ description } dangerouslySetInnerHTML={{ __html: desc }} />
        <span className={ displayLink }>{ link }</span>
      </div>
    </div>
  );
};


export default Card;
