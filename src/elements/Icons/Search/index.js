import React from 'react';


const SearchIcon = () => {
  return (
    <svg width="19px" height="19px" viewBox="0 0 19 19">
      <g stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
        <circle id="Oval" stroke="#000000" strokeWidth="2" cx="8" cy="8" r="7" />
        <path d="M13.290833,14.5431665 L17.3831664,14.5431665 C18.2600104,14.5431665 18.9708329,15.2539889 18.9708329,16.1308329 L18.9708329,16.1308329 C18.9708329,17.0076769 18.2600104,17.7184994 17.3831664,17.7184994 L13.290833,17.7184994 L13.290833,14.5431665 Z" id="Rectangle" fill="#000000" transform="translate(16.130833, 16.130833) rotate(45.000000) translate(-16.130833, -16.130833) " />
      </g>
    </svg>
  );
};


export default SearchIcon;
