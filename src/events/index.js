export const performSearchRequested = term => {
  return {
    type: 'SEARCH_CHANNEL',
    data: {
      evt: 'PERFORM_SEARCH_REQUESTED',
      data: term
    }
  };
};

// export const updateCurrentSearchResults = term => {
//   return {
//     type: 'SEARCH_CHANNEL',
//     data: {
//       evt: 'UPDATE_CURRENT_SEARCH_RESULTS_REQUESTED',
//       data: term
//     }
//   };
// };


// export const updateCurrentSearchResults = term => {
//   return {
//     type: 'UPDATE_CURRENT_SEARCH_RESULTS_REQUESTED'
//   };
// };
