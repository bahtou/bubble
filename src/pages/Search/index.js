import React, { Component } from 'react';

import SearchBar from 'Components/SearchBar';
import SearchResults from 'Components/SearchResults';

import { column, container, hLine, searchBar, searchResults } from './styles.css';


class Search extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <main className={ container }>

        <aside className={ column }>
          One must acknowledge with cryptography no amount of violence will ever solve a math problem
        </aside>

        <section className={ searchBar }>
          <SearchBar />
        </section>

        <hr className={ hLine } />

        <section className={ searchResults }>
          <SearchResults />
        </section>

      </main>
    );
  }
}


export default Search;
