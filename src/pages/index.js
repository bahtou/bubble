import { hot } from 'react-hot-loader';
import React from 'react';
import { Provider } from 'react-redux';

import store from '../storeConfig';
import Search from './Search';


const App = () => {
  return (
    <Provider store={ store }>
      <Search />
    </Provider>
  );
}


export default hot(module)(App);
