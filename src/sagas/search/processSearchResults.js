import { call, put, select, take } from 'redux-saga/effects';
import cheerio from 'cheerio';

//actions
const PROCESS_SEARCH_RESULTS = 'PROCESS_SEARCH_RESULTS';
const SAVE_SEARCH_RESULTS = 'SAVE_SEARCH_RESULTS';


async function getListItems(doc) {
  const $ = cheerio.load(doc, {
    decodeEntities: false,
    normalizeWhitespace: true
  });
  const listItems = [];

  $('#ires ol .g').each((i, elm) => {
    const title = $(elm).find('.r a:not([class])').text();
    let link = $(elm).find('.r a').attr('href');
    const cite = $(elm).find('cite').text();
    const desc = $(elm).find('.st').html();

    if (link) {
      link = link.split('=')[1].replace('&sa', '');

      listItems.push({
        title,
        link,
        desc
      });
    }
  });

  return listItems;
}

function* processSearchResults() {
  while (true) {
    const { data } = yield take(PROCESS_SEARCH_RESULTS);

    try {
      const searchResultItems = yield call(getListItems, data);
      const term = yield select(state => state.searchTerm);

      yield put({ type: SAVE_SEARCH_RESULTS, data: { term, searchResultItems }});
    } catch (err) {
      console.log(err);
      debugger;
    }
  }
}


export default processSearchResults;
