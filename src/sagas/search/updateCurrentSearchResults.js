import { put, take } from 'redux-saga/effects';

//actions
const UPDATE_CURRENT_SEARCH_RESULTS_REQUESTED = 'UPDATE_CURRENT_SEARCH_RESULTS_REQUESTED';
const SAVE_TO_CURRENT_SEARCH_RESULTS = 'SAVE_TO_CURRENT_SEARCH_RESULTS';


function* updateCurrentSearchResults() {
  while (true) {
    const { data } = yield take(UPDATE_CURRENT_SEARCH_RESULTS_REQUESTED);

    yield put({ type: SAVE_TO_CURRENT_SEARCH_RESULTS, data });
  }
}


export default updateCurrentSearchResults;
