import { all, put, take } from 'redux-saga/effects';

import getSearchResults from './getSearchResults';
import processSearchResults from './processSearchResults';


const SEARCH_CHANNEL = 'SEARCH_CHANNEL';

function* pipeline() {
  while (true) {
    const { data:{ evt, data }} = yield take(SEARCH_CHANNEL);

    yield put({ type: evt, data });
  }
}


export default function* search() {
  yield all([
    pipeline(),
    getSearchResults(),
    processSearchResults()
  ]);
}
