import { call, fork, put, take } from 'redux-saga/effects';
import axios from 'axios';

import mockSearchResult from './mockSearchResults';


//actions
const SAVE_SEARCH_TERM = 'SAVE_SEARCH_TERM';
const PROCESS_SEARCH_RESULTS = 'PROCESS_SEARCH_RESULTS';
const PERFORM_SEARCH_REQUESTED = 'PERFORM_SEARCH_REQUESTED';


function* saveSearchTerm(term) {
  yield put({ type: SAVE_SEARCH_TERM, data: term });
}

function searchRequest(term) {
  return axios.get(`https://reactredux.app/search?q=${term}`)
    .then(res => {
      return res.data;
    });
}

function* getSearchResults() {
  while (true) {
    const { data:term } = yield take(PERFORM_SEARCH_REQUESTED);

    yield fork(saveSearchTerm, term);

    try {
      const response = yield call(searchRequest, term);
      // const response = mockSearchResult;
      yield put({ type: PROCESS_SEARCH_RESULTS, data: response });

    } catch (err) {
      console.log(err);
      debugger;
    }
  }
}


export default getSearchResults;
