import { all } from 'redux-saga/effects';

import searchChannel from './search';


export default function* sagaCoordinator() {
  yield all([
    searchChannel()
  ]);
}
