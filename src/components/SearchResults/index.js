import React, { Component } from 'react';
import { connect } from 'react-redux';

import Card from 'Elements/Card';
import { container } from './styles.css';


class SearchResults extends Component {
  render() {
    const { term, results} = this.props;

    if (!results[term]) return null;

    return (
      <div className={ container }>
        {
          results[term].map(({ title, link, desc }, i) => {
            link = decodeURIComponent(link);
            return <Card key={i} title={title} link={link} desc={desc} />;
          })
        }
      </div>
    );
  }
}


function mapStateToProps(state) {
  return {
    term: state.searchTerm,
    results: state.searchResults
  };
}

// function mapStateToProps(state) {
//   const { currentSearchResults } = state.searchResults;
//   return {
//     searchResults: currentSearchResults
//   };
// }

export default connect(mapStateToProps)(SearchResults);
