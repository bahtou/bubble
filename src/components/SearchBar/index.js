import React, { Component } from 'react';

import SearchIcon from 'Elements/Icons/Search';
import { container, searchButton, searchInput } from './styles.css';

import { connect } from 'react-redux';
import { performSearchRequested, updateCurrentSearchResults } from 'Events';


class SearchBar extends Component {
  constructor(props) {
    super(props);

    this.state = {
      term: ''
    };
  }

  handleChange = e => {
    const term = e.target.value;
    this.setState({ term });
  }

  performSearch = e => {
    const { term } = this.state;
    const { performSearchRequested } = this.props;
    e.preventDefault();

    if (!term) return;

    performSearchRequested(term);
  }

  handleKeyPress = e => {
    const { key, keyCode, type  } = e;
    const { term } = this.state;
    const { performSearchRequested } = this.props;
    e.preventDefault();

    if (key === 'Enter' && keyCode === 13 && term) {
      performSearchRequested(term);
    }
  }

  render() {
    const { term } = this.state;

    return (
      <div className={ container }>
        <input type="search" className={ searchInput } value={ term }
          onChange={ this.handleChange }
          onKeyUp={ this.handleKeyPress } />
        <button className={ searchButton } onClick={ this.performSearch }>
          <SearchIcon />
        </button>
      </div>
    );
  }
}


// function mapStateToProps(state) {
//   return {
//     searchResults: state.searchResults
//   };
// }

export default connect(null, {
  performSearchRequested
})(SearchBar);
