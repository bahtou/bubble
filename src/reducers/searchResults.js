import createReducer from './createReducer';


const SAVE_SEARCH_RESULTS = 'SAVE_SEARCH_RESULTS';
const SAVE_TO_CURRENT_SEARCH_RESULTS = 'SAVE_TO_CURRENT_SEARCH_RESULTS';
const initialState = {
  currentSearchResults: []
};

const actionHandlers = {
  // [SAVE_TO_CURRENT_SEARCH_RESULTS]: (state, action) => {
  //   const term = action.data;
  //   const currentSearchResults = state[term];

  //   return { ...state, currentSearchResults };
  // },

  [SAVE_SEARCH_RESULTS]: (state, action) => {
    const { term, searchResultItems } = action.data;

    return { ...state,
      currentSearchResults: searchResultItems,
      [term]: searchResultItems
    };
  }
};


export default createReducer(initialState, actionHandlers);
