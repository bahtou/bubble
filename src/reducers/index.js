import { combineReducers } from 'redux';

import searchResults from './searchResults';
import searchTerm from './searchTerm';


const rootReducer = combineReducers({
  searchTerm,
  searchResults
});


export default rootReducer;
