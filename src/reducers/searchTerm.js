import createReducer from './createReducer';

export const SAVE_SEARCH_TERM = 'SAVE_SEARCH_TERM';
export const PERFORM_SEARCH_REQUESTED = 'PERFORM_SEARCH_REQUESTED';


const initialState = '';

const actionHandlers = {
  [SAVE_SEARCH_TERM]: (state, action) => {
    return action.data;
  }
};


export default createReducer(initialState, actionHandlers);
