const parse5 = require('parse5');
const cheerio = require('cheerio');
const fs = require('fs/promises');


const fileName = 'react';

async function getListItems(doc) {
  const $ = cheerio.load(doc, {
    decodeEntities: false,
    normalizeWhitespace: true
  });
  const listItems = [];

  $('#ires ol .g').each((i, elm) => {
    const title = $(elm).find('.r a:not([class])').text();
    let link = $(elm).find('.r a').attr('href');
    const cite = $(elm).find('cite').text();
    const desc = $(elm).find('.st').html();

    if (link) {
      // console.log(link);
      link = link.split('=')[1].replace('&sa', '');
      // console.log(link);
      // console.log(cite);

      listItems.push({
        title,
        link,
        desc
      });
    }
  });
  console.log(listItems);

  return listItems;
}

async function readFile() {
  const doc = await fs.readFile(`./googDocs/q_${fileName}.html`, 'utf8');

  return doc;
}


async function main() {
  const doc = await readFile();
  const listItems = await getListItems(doc);

  await fs.writeFile(`${fileName}.js`, JSON.stringify(listItems), { encoding: 'utf8', flag: 'w'}, err => {
    console.log(err);
  });
}


main();
