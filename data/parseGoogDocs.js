const fs = require('fs/promises');

const bodyRegex = /<body\sclass="\w*"\sbgcolor="#\w*"\smarginheight="\d*"\smarginwidth="\d"\stopmargin="\d*">(?<all>.*)<\/body>/gmis;
const scriptTagsRegex = /<script[a-zA-Z0-9!@#$%\^&*)(+=._\-,\s"\/>{}:';\[\]|\?\\n]*<\/script>/g;
const iresRegex = /<div\sid="ires">(?<list>.*)<\/div>/g;

const fileName = 'q_react.html';

async function readFile() {
  const doc = await fs.readFile(`./googDocs/${fileName}`, 'utf8');

  return doc;
}

async function getTheBodyTagInnerContent(document) {
  const _body = bodyRegex.exec(document);

  // await fs.writeFile('./googDocs/bodyContent.html', _body.groups.all, { encoding: 'utf8', flag: 'w'});
  return _body.groups.all;
}

async function getResultList(document) {
  const result = iresRegex.exec(document);

  return result;
}

async function removeScriptTags(body) {
  let m;
  let count = 0;
  let _doc = body;

  const matches = _doc.match(scriptTagsRegex);

  for (const tag of matches) {
    _doc = _doc.replace(tag, '');
  }

  return fs.writeFile(`./googDocs/out_${fileName}`, _doc, { encoding: 'utf8', flag: 'w'});
}

async function main() {
  const doc = await readFile();

  const result = await getResultList(doc);

  const body = await getTheBodyTagInnerContent(doc);
  await removeScriptTags(body);
}

main();
