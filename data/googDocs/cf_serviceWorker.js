addEventListener('fetch', event => {
  console.log('event received');
  event.respondWith(fetchSearch(event.request));
});


let searchTerm = /q=([^&]*)/gi;
let bodyRegex = /<body[^>]*>((.|[\n\r])*)<\/body>/gmi;
let scriptTagsRegex = /<script[^>]*>(.*?)<\/script>/gmi;
let noScriptTagsRegex = /<noscript[^>]*>(.*?)<\/noscript>/gmi;


async function getBodyTag(document) {
  let _body = bodyRegex.exec(document);

  if (!_body) {
    console.log('regex bodyTag failed');
    _body = document.match(bodyRegex);
  }

  return _body[0];
}

async function removeScriptTags(body) {
  let _doc = body;

  const matches = _doc.match(scriptTagsRegex);

  if (!matches) {
    console.log('regex scriptTag failed');
    return body;
  }

  for (const tag of matches) {
    _doc = _doc.replace(tag, '');
  }

  return _doc;
}

async function removeNoScriptTags(body) {
  let _doc = body;

  const matches = _doc.match(noScriptTagsRegex);

  if (!matches) {
    console.log('regex noscriptTag failed');
    return body;
  }

  for (const tag of matches) {
    _doc = _doc.replace(tag, '');
  }

  return _doc;
}

async function getSearchTerm(url) {
  let term = searchTerm.exec(url);

  if (!term) {
    term = url.match(searchTerm)[0].split('=');
  }

  console.log('term', term);
  return term[1];
}

async function fetchSearch(request) {
  console.log('search request', request.url);
  const searchTerm = await getSearchTerm(request.url);
  const goog = `https://www.google.com/search?q=${searchTerm}`;
  const init = { method: 'GET' };
  let document;

  try {
    console.log('fetching', goog);
    const response = await fetch(goog, init);
    document = await response.text();
  } catch (err) {
    console.error('fetching', err);
    return new Response(err);
  }

  let bodyHTML = await getBodyTag(document);
  bodyHTML = await removeScriptTags(bodyHTML);
  bodyHTML = await removeNoScriptTags(bodyHTML);

  return new Response(bodyHTML);
}
